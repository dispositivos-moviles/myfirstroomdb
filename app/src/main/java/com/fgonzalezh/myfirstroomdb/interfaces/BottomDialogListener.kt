package com.fgonzalezh.myfirstroomdb.interfaces

interface BottomDialogListener {
    fun onClick(contentId: Long)
}